import superagent from 'superagent';
import { parseOrderBook } from './protocol.js';

const baseUrl = 'https://www.binance.com/api/v3/';

const buildUrl = (uri, params = {}) => {
    const url = new URL(`${baseUrl}${uri}`);
    const { searchParams } = url;

    for (const [key, value] of Object.entries(params)) {
        searchParams.set(key, value);
    }

    return url.toString();
};

export const fetchOrderBook = async (symbol, limit = 5000) => {
    const url = buildUrl('depth', { limit, symbol: symbol.toUpperCase() });
    try {
        const { body } = await superagent.get(url);
        return parseOrderBook(body);
    } catch (e) {
        console.log(e);
        throw e;
    }
};

export const fetchTradingSymbols = async function* () {
    const url = buildUrl('exchangeInfo');
    const { body } = await superagent.get(url);

    yield* body.symbols.map(({ symbol }) => symbol.toLowerCase());
};
