export default class OrderBook {
    #initialized = false;

    #lastUpdateId;
    #asks;
    #bids;

    constructor(lastUpdateId, asks, bids) {
        this.#lastUpdateId = lastUpdateId;

        this.#asks = asks;
        this.#bids = bids;

        this.#guard(asks, 'asks');
        this.#guard(bids, 'bids');
    }

    update = ({ firstUpdateId, finalUpdateId, updatedBids, updatedAsks }) => {
        const lastUpdateId = this.#lastUpdateId;

        if (!this.#initialized && (lastUpdateId + 1 < firstUpdateId || finalUpdateId < lastUpdateId + 1)) {
            return;
        }

        if (this.#initialized && firstUpdateId !== lastUpdateId + 1) {
            throw new Error(
                `It must be consecutive updates: current orderbook update id = ${lastUpdateId}, new depth update has id ${firstUpdateId}`
            );
        }

        this.#initialized = true;
        this.#lastUpdateId = finalUpdateId;

        this.#updateOrders(this.#asks, updatedAsks);
        this.#updateOrders(this.#bids, updatedBids);
    };

    #updateOrders(map, aggregates) {
        for (const [price, quantity] of aggregates) {
            if (1 * quantity) {
                map.set(price, quantity);
            } else {
                map.delete(price);
            }
        }
    }

    #guard(map, prop) {
        if (!(map instanceof Map)) {
            throw new TypeError(`${prop} must be an instance of Map`);
        }
    }

    asks = () => this.#asks;
    bids = () => this.#bids;
}
