import OrderBook from './order-book.js';

export const parseOrderBook = ({ lastUpdateId, bids, asks }) => new OrderBook(lastUpdateId, new Map(asks), new Map(bids));

export const parseDepthUpdate = ({ s, U, u, b, a }) => ({
    firstUpdateId: U,
    finalUpdateId: u,
    updatedBids: b,
    updatedAsks: a,
    symbol: s.toLowerCase(),
})