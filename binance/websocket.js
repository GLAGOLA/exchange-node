import WebSocket from 'ws';

let id = 1;

export const subscribeToOrderBookUpdates = (wsc, params) => new Promise(resolve => {
    wsc.send(
        JSON.stringify({
            method: 'SUBSCRIBE',
            id: id++,
            params: params.map(s => `${s.toLowerCase()}@depth`),
        }),
        undefined,
        resolve
    );
});

export const websocketConnect = async (uri = '') => {
    const wsc = new WebSocket(`wss://stream.binance.com:9443/ws${uri}`);
    await new Promise(resolve => wsc.on('open', resolve));
    return wsc;
};