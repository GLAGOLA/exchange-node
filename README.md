1. запуск демона `docker-compose up`
1. комманда на подключение к бирже `docker-compose exec node npm run connect`
1. комманда на остановку `docker-compose exec node npm run disconnect`
1. отслеживание процесса активности записи `docker-compose exec redis redis-cli monitor`