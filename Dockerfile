FROM node:14.7.0-alpine3.12

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./


RUN apk update \
    && apk --no-cache --virtual build-dependencies add \
    python2 \
    make \
    g++ \
    && npm install --no-optional \
    && npm cache clean --force \
    && apk del build-dependencies

USER node

COPY --chown=node:node . .

CMD [ "node", "index.js" ]