import _ from 'lodash';
import redisFactory from 'redis';
import WebSocket from 'ws';
import dotenv from 'dotenv';
import RSMQWorker from 'rsmq-worker';

import {
    fetchOrderBook,
    fetchTradingSymbols,
    subscribeToOrderBookUpdates,
    websocketConnect,
    parseDepthUpdate,
} from './binance/index.js';

import { slowDownIterator, chunkIterator } from './iterators.js';

dotenv.config();

let wsc;

const redis = redisFactory.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const worker = new RSMQWorker(process.env.QUEUE_NAME, { redis, interval: 0.1 });

let active = true;
const shutdown = () => (active && process.kill(process.pid, 'SIGINT'), (active = false));

const prepareOrderBookForStore = orderBook => {
    const rawBids = [...orderBook.asks()];
    const rawAsks = [...orderBook.bids()];

    const cmp = ([a], [b]) => (a === b ? 0 : Math.sign(b - a));

    rawBids.sort(cmp);
    rawAsks.sort(cmp);

    return {
        asks: rawAsks,
        bids: rawBids,
    };
};

const connect = async () => {
    wsc = await websocketConnect();

    wsc.on('close', (code, reason) => {
        console.debug(`WebSocket closed (#${code})${reason ? ` : ${reason}` : ''}`);
        shutdown();
    });

    wsc.on('error', error => {
        console.error(`WebSocket error: ${error}`);
        shutdown();
    });

    const firstOrderBookUpdateReceived = new Map();
    const throttleOrderBooksUpdate = new Map();
    const depthUpdatesBufferBySymbol = new Map();

    wsc.on('message', dataStr => {
        const rawData = JSON.parse(dataStr);
        const { e: type } = rawData;

        if ('depthUpdate' !== type) {
            return;
        }

        const data = parseDepthUpdate(rawData);
        const { symbol } = data;

        (firstOrderBookUpdateReceived.get(symbol) || _.noop)();

        const buffer = depthUpdatesBufferBySymbol.get(symbol) || [];
        buffer.push(data);
        depthUpdatesBufferBySymbol.set(symbol, buffer);
        (throttleOrderBooksUpdate.get(symbol) || _.noop)();
    });

    const orderBooks = new Map();
    for await (const chunkOfSymbols of slowDownIterator(chunkIterator(fetchTradingSymbols(), 1), 5000)) {
        chunkOfSymbols.forEach(async symbol => {
            throttleOrderBooksUpdate.set(
                symbol,
                _.throttle(() => {
                    const buffer = depthUpdatesBufferBySymbol.get(symbol);
                    const orderBook = orderBooks.get(symbol);

                    if (!orderBook || !buffer) {
                        return;
                    }

                    buffer.forEach(orderBook.update);
                    buffer.length = 0;

                    redis.set(symbol, JSON.stringify(prepareOrderBookForStore(orderBook)));
                }, 400)
            );

            // wait first update to fetch order book
            await new Promise(resolve => firstOrderBookUpdateReceived.set(symbol, resolve));

            orderBooks.set(symbol, await fetchOrderBook(symbol, 5000));
            (throttleOrderBooksUpdate.get(symbol) || _.noop)();

            console.log(`order book for ${symbol} fetched`);
        });

        await subscribeToOrderBookUpdates(wsc, chunkOfSymbols);
    }
};

process.on('SIGINT', () => {
    console.debug(`SIGINT received. Exiting`);
    worker.quit();

    if (wsc && (WebSocket.CONNECTING === wsc.readyState || WebSocket.OPEN === wsc.readyState)) {
        wsc.close();
    }
});

worker.on('message', (msg, next) => {
    // todo add state machine - prevent wrong order of jobs
    console.debug(`Message received: ${msg}`);

    switch (msg) {
        case 'connect':
            connect();
            break;
        case 'disconnect':
            shutdown();
            break;

        default:
            console.log(`Unknown message: ${msg}`);
            break;
    }

    next();
});

worker.on('error', (error, msg) => {
    console.error(`RSMQWorker ERROR: "${error}"\nMSG ID: ${msg.id}`);
    shutdown();
});

worker.on('ready', () => {
    console.log(`Ready to accept commands`);
});

worker.start();
