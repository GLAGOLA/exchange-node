export const slowDownIterator = async function* (iterator, milliseconds = 0) {
    for await (const item of iterator) {
        milliseconds > 0 && await new Promise(resolve => setTimeout(resolve, milliseconds));
        yield item;
    }
}

export const chunkIterator = async function* (iterator, chunkSize) {
    let buffer = [];

    for await (const item of iterator) {
        if (buffer.length < chunkSize) {
            buffer.push(item);
        } else {
            yield buffer;
            buffer.length = 0;
        }
    }

    if (buffer.length) {
        yield buffer;
    }
}